provider "aws" {
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
  region = "${var.aws_region}"
}

resource "aws_vpc" "vpc" {
  cidr_block = "${var.cidr_vpc}"
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags {
    "Environment" = "${var.environment_tag}"
  }
}

resource "aws_internet_gateway" "igw" {
  vpc_id = "${aws_vpc.vpc.id}"
  tags {
    "Environment" = "${var.environment_tag}"
  }
}

resource "aws_subnet" "first_subnet_public" {
  vpc_id = "${aws_vpc.vpc.id}"
  cidr_block = "${var.first_cidr_subnet}"
  map_public_ip_on_launch = "true"
  availability_zone = "${var.availability_zone_a}"
  tags {
    "Environment" = "${var.environment_tag}"
  }
}

resource "aws_route_table_association" "first_rta_subnet_public" {
  subnet_id      = "${aws_subnet.first_subnet_public.id}"
  route_table_id = "${aws_route_table.rtb_public.id}"
}

resource "aws_subnet" "second_subnet_public" {
  vpc_id = "${aws_vpc.vpc.id}"
  cidr_block = "${var.second_cidr_subnet}"
  map_public_ip_on_launch = "true"
  availability_zone = "${var.availability_zone_b}"
  tags {
    "Environment" = "${var.environment_tag}"
  }
}

resource "aws_route_table_association" "second_rta_subnet_public" {
  subnet_id      = "${aws_subnet.second_subnet_public.id}"
  route_table_id = "${aws_route_table.rtb_public.id}"
}

resource "aws_route_table" "rtb_public" {
  vpc_id = "${aws_vpc.vpc.id}"
  route {
      cidr_block = "0.0.0.0/0"
      gateway_id = "${aws_internet_gateway.igw.id}"
  }tags {
    "Environment" = "${var.environment_tag}"
  }
}

resource "aws_security_group" "sg_22" {
  name = "sg_22"
  vpc_id = "${aws_vpc.vpc.id}"  
  
  ingress {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp" 
      cidr_blocks = ["0.0.0.0/0"]
  } 

  ingress {
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  } 
  
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }  
  
  tags {
    "Environment" = "${var.environment_tag}"
  }
}

resource "aws_launch_template" "cowsay" {
  name_prefix   = "cowsay"
  image_id      = "${var.cowsay_aws_ami}"
  instance_type = "t2.micro"
  vpc_security_group_ids = ["${aws_security_group.sg_22.id}"]
  key_name = "${var.aws_key_pair}"

  tags = {
    Name = "cowsay"
    Environment = "${var.environment_tag}"
  }
}

resource "aws_autoscaling_group" "cowsay_autoscaling" {
  vpc_zone_identifier = ["${aws_subnet.first_subnet_public.id}", "${aws_subnet.second_subnet_public.id}"]
  desired_capacity   = 1
  max_size           = 2
  min_size           = 1

  launch_template {
    id      = "${aws_launch_template.cowsay.id}"
    version = "$Latest"
  }
}