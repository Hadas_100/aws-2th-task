
variable "access_key" {
  default     = "AKIA56IGYK56JHTBT4GB"
}

variable "secret_key" {
  default     = "OEwtLSkySkDrU11fVugwIxHBXeoqOrEgJRKeA2ee"
}

variable "aws_key_pair" {
  description = "Public key pair"
  default = "dev-server-1"
}

variable "aws_region" {
  description = "AWS region to launch servers."
  default     = "us-east-2"
}

variable "availability_zone_a" {
  description = "availability zone to create subnet"
  default = "us-east-2a"
}

variable "availability_zone_b" {
  description = "availability zone to create subnet"
  default = "us-east-2b"
}

variable "cidr_vpc" {
  description = "CIDR block for the VPC"
  default = "10.1.0.0/16"
}

variable "first_cidr_subnet" {
  description = "CIDR block for the subnet"
  default = "10.1.1.0/24"
}

variable "second_cidr_subnet" {
  description = "CIDR block for the subnet"
  default = "10.1.2.0/24"
}

variable "cowsay_aws_ami" {
  default = "ami-09ca109ad981719ce"
}

variable "environment_tag" {
  description = "Environment tag"
  default = "Production"
}

 